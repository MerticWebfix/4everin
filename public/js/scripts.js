$('document').ready(function () {

    let url = window.location.href;
    let res = url.split("/");
    let value = parseInt(res[res.length - 1]);

    if (value && value > 0) {

        $('html, body').animate({
            scrollTop: $("#orderForm").offset().top
        }, 2000);
    }


    $('.item').on({

        mouseover: function () {

            if ($(this).hasClass('selected')) {
                $(this).find('.selected-icon').hide();
                $(this).find('.body').css('opacity', '0.2');
                if ($(this).find('.body').is(':visible')) {
                    $(this).find(".link-red-remove").stop().show();
                    $(this).find(".link-red-remove").show();
                }
            } else {
                $(this).find('.body').css('opacity', '0.2');
                if ($(this).find('.body').is(':visible')) {
                    $(this).find(".link-red-addd").stop().show();
                }
            }
        },

        mouseout: function () {

            if ($(this).hasClass('selected')) {
                $(this).find('.selected-icon').show();
                $(this).find(".link-red-remove").hide();
            } else {
                $(this).find('.body').css('opacity', '1');
                $(this).find(".link-red-addd").stop().hide();
            }
        }
    })

    $('.eshop-select').on('click', function () {
        let id = $(this).attr('data-id');
    });

    $('.hamburger').on('click', function () {
        $('.mobile-menu').slideToggle('slow');
    });

    $('#close-menu').on('click', function () {
        $('.mobile-menu').slideToggle('slow');
    });

    $('.arrow-left').on('click', function () {
        let image = $('.image-slider:first');

        $('.images').append(image);

    });

    $('.arrow-right').on('click', function () {
        let image = $('.image-slider:last');

        $('.images').prepend(image);

    });

    $('#order_form_withoutText').on('click', function () {
        $('.step-4').toggle();
    });

    $('#order_form_company').on('click', function () {
        $('#nakupNaFirmu').toggle();
    });

    $('#order_form_billingAddress').on('click', function () {
        $('#fakturacniAdresa').toggle();
    });


    $('.view-orig').on({

        mouseover: function () {

            let width = screen.width;
            if(width > 800) {
                $(this).parent().find('.item-wrap').addClass('sample-div');
                $(this).parent().find('.body').hide();
                $(this).parent().find('.link-red-addd').hide();
                $(this).parent().find('.sample').show();
            }

        },

        mouseout: function () {
            let width = screen.width;
            if(width > 800) {
                $(this).parent().find('.item-wrap').removeClass('sample-div');
                $(this).parent().find('.sample').hide();
                $(this).parent().find('.body').show();
            }
        }
    })

    $('.close-modal').on('click', function () {
        $(this).closest('.modal-potvrzeni').hide();
    });


    $('.view-orig').on('click', function (){


        if($(this).parent().find('.sample').is(':visible')){


            $(this).parent().find('.item-wrap').removeClass('sample-div');
            $(this).parent().find('.sample').hide();
            $(this).parent().find('.body').show();
            $(this).parent().find('.body').css('opacity', '1');

        }
        else{
            $(this).parent().find('.item-wrap').addClass('sample-div');
            $(this).parent().find('.body').hide();
            $(this).parent().find('.link-red-addd').hide();
            $(this).parent().find('.sample').show();
        }

    });

    document.getElementById('order_form_file').onchange = function () {
        let path = document.getElementById('order_form_file').value;//take path
        let tokens= path.split('\\');//split path
        let filename = tokens[tokens.length-1];//take file name

        $('#fakeUploadInput').val(filename);
    };
});
