<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Admin\Fields\VichImageField;
use App\Entity\Order\FontType;
use App\Entity\Reference;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FontTypeCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return FontType::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Fonty')
            ->setEntityLabelInSingular('Font')
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name')->setLabel('Název'),
            VichImageField::new('imageFile')->onlyOnForms()->setLabel('Náhled')
        ];
    }
}
