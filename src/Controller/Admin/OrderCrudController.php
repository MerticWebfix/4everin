<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Admin\Fields\VichImageField;
use App\Entity\Order;
use App\Entity\Reference;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class OrderCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Order::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Objednávky')
            ->setEntityLabelInSingular('Objednávka')
            ->setSearchFields(['products.name', 'id', 'client.email'])
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_DETAIL, Action::EDIT)
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(BooleanFilter::new('processed', 'Zpracováno'))
            ->add(EntityFilter::new('products', 'Produkty'))
            ;
    }

    public function configureFields(string $pageName): iterable {
        return [
            IdField::new('id', 'Číslo objednávky')->hideOnForm(),
            DateField::new('createdAt', 'Datum'),
            BooleanField::new('processed', 'Zpracováno'),
            MoneyField::new('totalPrice', 'Cena')->setCurrency('CZK')->setNumDecimals(0)->setStoredAsCents(false),
            TextField::new('note', 'Poznámka')->onlyOnDetail(),

            FormField::addPanel('Definice produktu'),
            CollectionField::new('products', 'Produkty'),
            NumberField::new('itemsCount', 'Počet kusů'),
            ImageField::new('photo', 'Foto')->onlyOnDetail()->setBasePath('/soubory/objednavky/'),
            BooleanField::new('photo3D', '3D')->renderAsSwitch(false),
            BooleanField::new('withText', 'S textem')->renderAsSwitch(false),
            TextField::new('text', 'Text')->onlyOnDetail(),
            AssociationField::new('font', 'Font')->onlyOnDetail(),

            FormField::addPanel('Doprava a platba'),
            AssociationField::new('delivery', 'Doprava')->onlyOnDetail(),
            TextField::new('packeteryBranch', 'Pobočka zásilkovny')->onlyOnDetail(),
            AssociationField::new('payment', 'Platba')->onlyOnDetail(),

            FormField::addPanel('Klient'),
            TextField::new('client.name', 'Jméno a příjmení'),
            TextField::new('client.phone', 'Telefon'),
            TextField::new('client.email', 'Email'),

            TextField::new('client.companyName', 'Firma')->onlyOnDetail(),
            TextField::new('client.ein', 'IČO')->onlyOnDetail(),
            TextField::new('client.vatNumber', 'DIČ')->onlyOnDetail(),

            FormField::addPanel('Doručovací adresa'),
            TextField::new('client.address.street', 'Ulice a č.p.')->onlyOnDetail(),
            TextField::new('client.address.city', 'Město')->onlyOnDetail(),
            TextField::new('client.address.postCode', 'PSČ')->onlyOnDetail(),

            FormField::addPanel('Fakturační adresa'),
            TextField::new('client.billingAddress.street', 'Ulice a č.p.')->onlyOnDetail(),
            TextField::new('client.billingAddress.city', 'Město')->onlyOnDetail(),
            TextField::new('client.billingAddress.postCode', 'PSČ')->onlyOnDetail(),
        ];
    }
}
