<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homme")
     */
    public function homeAction()
    {
        $seo = [
            'description' => 'Netradiční dárek z Vašich fotografií',
            'keywords' => 'fotky ve skle, foto ve skle, dárek z fotek, foto ve skle 2D, foto ve skle 3D, laserování do skla'
        ];
        return $this->render('home.html.twig', $seo);
    }

    /**
     * @Route("/hlavni-stranka", name="main")
     */
    public function mainAction() {
        $seo = [
            'description' => 'Netradiční dárek z Vašich fotografií',
            'keywords' => 'fotky ve skle, foto ve skle, dárek z fotek, foto ve skle 2D, foto ve skle 3D, laserování do skla'
        ];
        return $this->render('home.html.twig', $seo);
    }

    /**
     * @Route("/kontakt", name="contact")
     */
    public function contactAction()
    {
        $params = [
            'description' => 'Kontakt 4EVERIN sklo jinak',
            'keywords' => '4everin, laserování do skla, gravírování do skla',
            'title' => 'Kontakt'
        ];
        return $this->render('contact.html.twig', $params);
    }

    /**
     * @Route("/firmy", name="companies")
     */
    public function companiesAction()
    {
        $seo = [
            'description' => 'Výroba upomínkových předmětů ze skla',
            'keywords' => 'logo ve skle, reklamní skleněné předměty, gravírování do skla,vaše fotky ve skle 2D/3D - fotky ve skle, foto ve skle, dárek z fotek, 2D/3D fotografie ve skle',
            'title' => 'Pro firmy'

        ];
        return $this->render('companies.html.twig', $seo);
    }

    /**
     * @Route("/fotky", name="photos")
     */
    public function photosAction()
    {
        $seo = [
            'description' => 'Vaše fotky ve skle 2D/3D',
            'keywords' => 'fotky ve skle, foto ve skle, dárek z fotek, 2D/3D fotografie ve skle',
            'title' => 'Vaše fotky'
        ];
        return $this->render('photos.html.twig', $seo);
    }

    /**
     * @Route("/zakazka", name="custom")
     */
    public function customAction()
    {
        $seo = [
            'description' => 'Zakázková výroba - foto ve skle',
            'keywords' => 'foto ve skle, skleněné dárky, 2D/3D fotografie ve skle',
            'title' => 'Zakázková výroba'
        ];
        return $this->render('custom.html.twig', $seo);
    }


    /**
     * @Route("/obchodni-podminky", name="obchodniPodminky")
     */
    public function obchodniPodminkyAction()
    {
        $seo = [
            'description' => 'Obchodní podmínky 4EVERIN sklo jinak',
            'keywords' => '4everin, laserování do skla, gravírování do skla'
        ];
        return $this->render('obchodniPodminky.html.twig', $seo);
    }

    /**
     * @Route("/reklamace", name="reklamace")
     */
    public function reklamaceAction()
    {
        $seo = [
            'description' => 'Reklamace 4EVERIN sklo jinak',
            'keywords' => '4everin, laserování do skla, gravírování do skla'
        ];
        return $this->render('reklamace.html.twig', $seo);
    }

    /**
     * @Route("/ochrana-osobnich-udaju", name="gdpr")
     */
    public function gdprAction()
    {
        $seo = [
            'description' => 'Zpracování osobních údajů 4EVERIN sklo jinak',
            'keywords' => '4everin, laserování do skla, gravírování do skla'
        ];
        return $this->render('gdpr.html.twig', $seo);
    }
}
