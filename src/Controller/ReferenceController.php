<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\ClientReference;
use App\Entity\Reference;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ReferenceController extends AbstractController {

    /**
     * @Route("/reference", name="reference")
     */
    public function referenceAction()
    {
        $referenceRepository = $this->getDoctrine()->getRepository(Reference::class);
        $clientsRepository = $this->getDoctrine()->getRepository(ClientReference::class);

        $clients = $clientsRepository->findAll();
        $reference = $referenceRepository->findAll();

        return $this->render(
            'references.html.twig',
            [
                'reference' => $reference,
                'clients' => $clients,
                'title' => 'Reference',
                'description' => 'Reference 4EVERIN sklo jinak',
                'keywords' =>  'fotky ve skle, logo ve skle, reference fotografie ve skle'
            ]
        );
    }
}
