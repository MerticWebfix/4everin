<?php declare(strict_types=1);

namespace App\Entity\Order\Client;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Address {

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $street;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $city;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $postCode;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getStreet(): string {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getCity(): string {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPostCode(): string {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     */
    public function setPostCode(string $postCode): void {
        $this->postCode = $postCode;
    }
}
