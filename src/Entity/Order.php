<?php declare(strict_types=1);

namespace App\Entity;

use App\Entity\Order\Client;
use App\Entity\Order\DeliveryType;
use App\Entity\Order\FontType;
use App\Entity\Order\PaymentType;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity()
 * @Vich\Uploadable()
 * @ORM\Table(name="order_table")
 */
class Order {
    use TimestampableEntity;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $processed = false;

    /**
     * @var Product[]
     * @ORM\ManyToMany(targetEntity="Product")
     * @JoinTable(name="ordered_products",
     *      joinColumns={@JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="product_id", referencedColumnName="id")}
     *      )
     */
    protected $products;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $itemsCount = 1;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false)
     */
    protected $totalPrice = 0.0;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @var string|null
     */
    protected $photo;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $photo3D = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $withText = false;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $text = null;

    /**
     * @var FontType|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Order\FontType")
     * @ORM\JoinColumn(name="font_id", referencedColumnName="id", nullable=true)
     */
    protected $font = null;

    /**
     * @var PaymentType
     * @ORM\ManyToOne (targetEntity="App\Entity\Order\PaymentType")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id", nullable=false)
     */
    protected $payment;

    /**
     * @var DeliveryType
     * @ORM\ManyToOne(targetEntity="App\Entity\Order\DeliveryType")
     * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id", nullable=false)
     */
    protected $delivery;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $packeteryBranchName;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $packeteryBranchId;

    /**
     * @var Client
     * @ORM\OneToOne(targetEntity="App\Entity\Order\Client", cascade={"persist", "remove"}, orphanRemoval=true)
     * @JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    protected $client;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    protected $note;

    public function __construct() {
        $this->products = new ArrayCollection();
        $this->updatedAt = new DateTime();
        $this->createdAt = new DateTime();
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection {
        return $this->products;
    }

    /**
     * @param Collection $projectImages
     */
    public function setProducts(Collection $products): void {
        foreach ($products as $product) {
            $this->addProduct($product);
        }
    }

    public function addProduct(Product $product) {
        $this->products[] = $product;
        $this->updatedAt = new DateTime('now');
    }

    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isProcessed(): bool {
        return $this->processed;
    }

    /**
     * @param bool $processed
     */
    public function setProcessed(bool $processed): void {
        $this->processed = $processed;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     */
    public function setTotalPrice(float $totalPrice): void {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return string|null
     */
    public function getPhoto(): ?string {
        return $this->photo;
    }

    /**
     * @param string|null $photo
     */
    public function setPhoto(?string $photo): void {
        $this->photo = $photo;
    }

    /**
     * @return bool
     */
    public function isPhoto3D(): bool {
        return $this->photo3D;
    }

    /**
     * @param bool $photo3D
     */
    public function setPhoto3D(bool $photo3D): void {
        $this->photo3D = $photo3D;
    }

    /**
     * @return bool
     */
    public function isWithText(): bool {
        return $this->withText;
    }

    /**
     * @param bool $withText
     */
    public function setWithText(bool $withText): void {
        $this->withText = $withText;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void {
        $this->text = $text;
    }

    /**
     * @return FontType|null
     */
    public function getFont(): ?FontType {
        return $this->font;
    }

    /**
     * @param FontType|null $font
     */
    public function setFont(?FontType $font): void {
        $this->font = $font;
    }

    /**
     * @return PaymentType
     */
    public function getPayment(): PaymentType {
        return $this->payment;
    }

    /**
     * @param PaymentType $payment
     */
    public function setPayment(PaymentType $payment): void {
        $this->payment = $payment;
    }

    /**
     * @return DeliveryType
     */
    public function getDelivery(): DeliveryType {
        return $this->delivery;
    }

    /**
     * @param DeliveryType $delivery
     */
    public function setDelivery(DeliveryType $delivery): void {
        $this->delivery = $delivery;
    }

    /**
     * @return Client
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client): void {
        $this->client = $client;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string {
        return $this->note;
    }

    /**
     * @param string|null $note
     */
    public function setNote(?string $note): void {
        $this->note = $note;
    }

    /**
     * @return int
     */
    public function getItemsCount(): int {
        return $this->itemsCount;
    }

    /**
     * @param int $itemsCount
     */
    public function setItemsCount(int $itemsCount): void {
        $this->itemsCount = $itemsCount;
    }

    public function updateTotalPrice() {
        $price = 0;
        /** @var Product $product */
        foreach ($this->getProducts() as $product) {
            $price += $product->getDiscountPrice() ?: $product->getPrice();
            if ($this->photo3D) {
                $price += $product->getLaser3DPrice();
            }
        }

        $price = $this->itemsCount * $price;

        if ($this->getPayment()) {
            $price += $this->getPayment()->getPrice();
        }

        if ($this->getDelivery()) {
            $price += $this->getDelivery()->getPrice();
        }

        $this->totalPrice = $price;
    }

    /**
     * @return string|null
     */
    public function getPacketeryBranchName(): ?string {
        return $this->packeteryBranchName;
    }

    /**
     * @param string|null $packeteryBranchName
     */
    public function setPacketeryBranchName(?string $packeteryBranchName): void {
        $this->packeteryBranchName = $packeteryBranchName;
    }

    public function getPacketeryBranchId(): ?int {
        return $this->packeteryBranchId;
    }

    public function setPacketeryBranchId(?int $packeteryBranchId): void {
        $this->packeteryBranchId = $packeteryBranchId;
    }

    public function getPacketeryBranch(): ?string {
        $str = null;
        if ($this->packeteryBranchId) {
            $str = $this->packeteryBranchName. ' ('.$this->packeteryBranchId.')';
        }

        return $str;
    }
}
