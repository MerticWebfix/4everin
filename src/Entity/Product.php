<?php declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class Product {
    use TimestampableEntity;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=false, length=100)
     */
    protected $name;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=false, length=100)
     */
    protected $size;

    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=false)
     */
    protected $price = 0.0;

    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    protected $discountPrice = null;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $allowed3DLaser = false;

    /**
     * @var float|null
     * @ORM\Column(type="float", nullable=true)
     */
    protected $laser3DPrice = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $sampleImage;

    /**
     * @Vich\UploadableField(mapping="product_sample_image", fileNameProperty="sampleImage")
     * @var File
     */
    private $sampleImageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $realImage;

    /**
     * @Vich\UploadableField(mapping="product_real_image", fileNameProperty="realImage")
     * @var File
     */
    private $realImageFile;


    public function __construct() {
        $this->setCreatedAt(new DateTime());
        $this->setUpdatedAt(new DateTime());
    }


    /**
     * @return int
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getSize(): ?string {
        return $this->size;
    }

    /**
     * @param string|null $size
     */
    public function setSize(?string $size): void {
        $this->size = $size;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void {
        $this->price = $price;
    }

    /**
     * @return float|null
     */
    public function getDiscountPrice(): ?float {
        return $this->discountPrice;
    }

    /**
     * @param float|null $discountPrice
     */
    public function setDiscountPrice(?float $discountPrice): void {
        $this->discountPrice = $discountPrice;
    }

    /**
     * @return bool
     */
    public function isAllowed3DLaser(): bool {
        return $this->allowed3DLaser;
    }

    /**
     * @param bool $allowed3DLaser
     */
    public function setAllowed3DLaser(bool $allowed3DLaser): void {
        $this->allowed3DLaser = $allowed3DLaser;
    }

    /**
     * @return float|null
     */
    public function getLaser3DPrice(): ?float {
        return $this->laser3DPrice;
    }

    /**
     * @param float|null $laser3DPrice
     */
    public function setLaser3DPrice(?float $laser3DPrice): void {
        $this->laser3DPrice = $laser3DPrice;
    }

    public function setSampleImageFile(File $image = null)
    {
        $this->sampleImageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getSampleImageFile()
    {
        return $this->sampleImageFile;
    }

    public function setSampleImage($image)
    {
        $this->sampleImage = $image;
    }

    public function getSampleImage()
    {
        return $this->sampleImage;
    }

    public function setRealImageFile(File $image = null)
    {
        $this->realImageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getRealImageFile()
    {
        return $this->realImageFile;
    }

    public function setRealImage($image)
    {
        $this->realImage = $image;
    }

    public function getRealImage()
    {
        return $this->realImage;
    }

    public function __toString() {
       return $this->name.' '.$this->size.' mm';
    }


}
