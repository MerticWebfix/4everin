<?php declare(strict_types=1);


namespace App\Service;

use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use DomainException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class OrderMailSender {

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SignUpMailSender constructor.
     * @param MailerInterface        $mailer
     * @param Environment            $twig
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(MailerInterface $mailer, Environment $twig, EntityManagerInterface $entityManager) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    public function send(Order $order) {
        $email = $this->createEmail($order);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            throw new DomainException('Nepodařilo se odeslat objednávku.');
        }

    }

    public function sendTestMail() {
        try {
            $email = new Email();
            $ak = new Address('objednavky@4everin.cz', '4everin.cz');
            $email->from($ak);
            $email->replyTo($ak);
            $email->to(new Address('jmerta@outlook.com'));
            $email->subject('Test emailu');
            $email->text('asdasdasd');

            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            throw new DomainException($e->getMessage());
        }
    }

    private function createEmail(Order $order): Email {

        $email = new Email();
        $ak = new Address('objednavky@4everin.cz', '4everin.cz');
        $cc = new Address('obchod@4everin.cz');
        $email->from($ak);
        $email->replyTo($cc);
        $email->cc($cc);

        $this->resolveRecipients($order, $email);
        $email->subject('4everin.cz - Objednávka č. '.$order->getId());

        $email->html($this->renderMailContent($order));

        return $email;
    }

    /**
     * @param Order $order
     * @param Email $email
     */
    private function resolveRecipients(Order $order, Email $email): void {

        if ($order->getClient() && $order->getClient()->getEmail()) {
            $email->to(new Address($order->getClient()->getEmail()));
        }
    }

    /**
     * @param Order $order
     * @return string
     */
    private function renderMailContent(Order $order): string {
        return $this->twig->render('mail.html.twig', ['order' => $order]);
    }

}
